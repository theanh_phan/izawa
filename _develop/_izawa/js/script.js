$(document).ready(function() {
  $(".main-slider").slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    fade: true,
    autoplaySpeed: 6000,
    dots: false,
    arrows: false,
  });
});