<header>
  <div class="gr_header">
    <h1 class="gr_header_logo"><a href="/"><img src="/common/images/logo_pc.png" alt="Logo"></a></h1>
    <div id="icon_nav" class="gr_header_icon show_sp">
    	<div class="gr_header_icon_l">
    		<em>MENU</em>
    	</div>
    	<!--/.left-->
			<div class="icon_menu">
        <div class="icon_inner"></div>
      </div>
    </div>
    <nav id="nav_menu" class="gr_header_nav">
    	<ul>
    		<li><a href="#">会社概要</a>
				<li><a href="#">井澤徳について</a>
				<li><a href="#">事例紹介</a>
				<li><a href="#">CSR</a>
				<li><a href="#">採用情報</a>
				<li><a href="#">インターンブログ</a>
				<li><a href="#">お問い合わせ</a>
				<li><a href="#">コラム</a>
    	</ul>
    </nav>
    <!--/.nav_menu-->
  </div>
  <!--/.gr_header-->
</header>